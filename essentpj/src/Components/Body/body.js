import React from 'react';
import './body.css'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import mom_baby_sit from '../../Images/mom_baby_sit.jpg'; 
import table_wine from '../../Images/table_wine.jpg'; 
import nest_learning from '../../Images/Nest_learning.jpg'; 
import family_cook from '../../Images/family_cook.jpg'; 
import game_playing from '../../Images/game_playing.jpg'; 
import dad_kid from '../../Images/Dad_kid.jpg'; 
import two_women from '../../Images/2women.jpg'; 
import gragh from '../../Images/graph_icon.jpg'; 
import Footer_logo from '../../Images/footer_logo.jpg'; 
import Button from 'react-bootstrap/Button'

function Body() {
  return (
    <>
      <div  className="bdy_contain">

          <h1 className="hotpink">
          Gebruik deze tips
          </h1>
           <p>Geachte mevrouw Jansen,</p>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
      
<hr style={{
  marginTop:"50px",
  marginBottom:"50px",
            height: 1,
            backgroundColor: "#d8d8d8",
    height: "1px",
        }}
    />

  <Row sm={1} >
    <Col lg={true}>
      <h1 className="mb_title">75 euro korting <br/>op ou energie</h1>
      <img src={mom_baby_sit} alt="mom and baby" className="mini_img" />
      <ul className="bullets" style={{marginTop:"10px", }}>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
</ul>
<h5 className="h5">Bekijk het aanbod</h5>
<div className="btn_cntre">
<Button size="lg" className="mb_btn" style={{width:"320px", height:"60px",background:"#489db6",marginTop: "10px",}}>
    Ik kies 75 euro korting
  </Button>
  </div>
    </Col>
    <Col lg={true}>
      <h1 className="mb_title">Media Markt <br/>cadeaubon</h1>
      <img src={table_wine} alt="family table" className="mini_img" />

<p className="media_markt">Lorem ipsum dolor sit amet,
consectetur adipisicing elit, sed do
eiusmod tempor incididunt ut
labore et dolore magna aliqua. Ut
enim ad minim veniam, quis nostrud</p>

<h5 className="h5">Bekijk het aanbod</h5>
<div className="btn_cntre">
<Button size="lg" className="mb_btn"  style={{width:"320px", height:"60px",background:"#489db6", marginTop: "25px"}}>
    Ik kies een cadeaubon
  </Button>
  </div>
      </Col>

    <Col lg={true}>
      <h1 className="mb_title">
        Nest Learning <br/>Thermostat<sup style={{top: "-18.5px",fontSize:" 15px"}}>TM</sup>
      </h1>
      <img src={nest_learning} alt="nest learning" className="mini_img" />
      <ul className="bullets" style={{marginTop:"10px", }}>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
  <li>Lorem ipsum dolor sit amet.</li>
</ul>
<h5 className="h5">Bekijk het aanbod</h5>
<div className="btn_cntre">
<Button size="lg" className="mb_btn"  style={{width:"320px", height:"60px",background:"#489db6", marginTop: "10px"}} >
    Ik kies Nest cadeau
  </Button>
  </div>
        </Col>
  </Row> 
  </div>


 <div>
   <div className="img_text_container">
   <h1 className="img_text1">ketel:het warme hart</h1>
   <h1 className="img_text2">van uw huis</h1>

   <div className="img_btn">
   <Button size="lg" className="mb_btn"  style={{width:"200px", height:"60px", background:"#489db6"}} >
Lorem ipsum
  </Button>
  </div>
   </div>
   <div>
 <img src={family_cook} alt="family cooking" className="nd_img" />
 <p className="mb_img_txt">CV-Ketel: <br/>het warme hart van uw huis ></p>
</div>

 <Row>
    <Col  lg={true}>
      <div className="ctn">
    <img src={game_playing} alt="men gaming" className="ctn_img"/>   
    </div> 
    </Col>
    <Col >
    <h1  style={{color:"#e82b7d"}}>CV-ketel:het warme hart <br/>van uw huis</h1>
    <div className="pad_space">
    <p>Uw jas drogen na een flinke regenbui en vervolgens
onder een heerlijk warme douche stappen. Daar
zorgt een goede -ketel voor. We vertellen u graag
meer over verwarming en onderhoud.</p>

<p>
Lorem ipsum dolor sit amet, consectetuer adipiscing
elit. Aenean commodo ligula eget dolor. Aenean
massa. </p>
</div>
<div className="btn_cntre">
<Button size="lg" className="mb_btn" style={{width:"200px", height:"60px", background:"#489db6" }} >
Lorem ipsum
  </Button>
  </div>
    </Col>
  
  </Row>
  <hr style={{
            height: 1,
            backgroundColor: "#d8d8d8",
    height: "1px",
    margin:" 50px 100px 50px 100px",
    paddingLeft: "40px",
        }}
    />
    
 <Row className="item_view">
    <Col lg={true} md={{ order: 'first' }} lg={{ order: 'first' }}  >
    <div className="pad_space_nd">
    <h1 style={{color:"#e82b7d"}}>Isolatie: Koel in de zomer,<br/> warm in de winter </h1>
    <p className="pad_space">
    Na een snikhete dag thuiskomen in een heerlijk koel
huis? Of in de wintermaanden een huis dat heerlijk op
temperatuur blijft, zelfs de zolderkamer? Goede
isolatie zorgt daar absoluut voor.
    </p>
    <div className="btn_cntre">
    <Button size="lg" className="mb_btn" style={{width:"200px", height:"60px", background:"#489db6"}} >
Lorem ipsum
  </Button>
  </div>

  </div>

    </Col>
    <Col sm={{ order: 'first' }} xs={{ order: 'first' }} >
      <div className="ctn">
    <img src={dad_kid} alt="dad and daughter" className="ctn_img" />
    </div>
    </Col>

  </Row>
  <hr style={{
            height: 1,
            backgroundColor: "#d8d8d8",
    height: "1px",
    margin:" 50px 100px 50px 100px",
    paddingLeft: "40px",
        }}
    />
  <Row  className="space">
    <Col  lg={true}>
    <div className="ctn">
    <img src={two_women} alt="women shopping" className="ctn_img"/>   
</div> 
    </Col>
    <Col >
    <h1  style={{color:"#e82b7d"}}> Tijdelijk 300 euro cadeau <br/>bij Vast & Zeker 3 jaar</h1>
    <div className="pad_space">
<p>Uw jas drogen na een flinke regenbui en vervolgens
onder een heerlijk warme douche stappen. Daar
zorgt een goede -ketel voor. We vertellen u graag
meer over verwarming en onderhoud.</p>
<p>
Lorem ipsum dolor sit amet, consectetuer adipiscing
elit. Aenean commodo ligula eget dolor. Aenean
massa.</p>
</div>
<div className="btn_cntre">
<Button size="lg" className="mb_btn" style={{width:"200px", height:"60px", background:"#489db6"}} >
Lorem ipsum
  </Button>
  </div>
    </Col>
  </Row>
  <hr style={{
            height: 1,
            backgroundColor: "#d8d8d8",
    height: "1px",
    margin:" 50px 0 50px 0px",
    paddingLeft: "40px",
        }}
    />
  <div className="space">

  <Row className="footer_one">
    <Col sm={8}>
      <h1>Dalende olie- en gasprijzen</h1>
      <div className="mble_size">
      <p style={{paddingRight:100}}>
      De prijs van kolen, olie en gas dalen flink. De oorzaken: het aanhoudend
      warme winterweer, de grote voorraden én de economische afkoeling in
      China.
      </p>
      <h5 className="h5">Lees meer</h5>
      </div>
    </Col>
    <Col lg={true} >
      <div className="graph_size">
    <img src={gragh} alt="graph" className="graph_img"/>
    </div>
    </Col>
  </Row>
  </div>
  <div className="last_image">
    <div className="last_img">
  <img src={Footer_logo} alt="footer image" className="footer_logo"/>
  </div>
  </div>
 </div>
</>
  );
}

export default Body;
