import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import logo from '../../Images/main_logo.jpg'
import './nav.css'

function NavBar() {
  return (
    <>
    <Navbar expand="lg" variant="light" sticky="top" bg="white" style={{height:"120px"}}>
    <Container style={{marginLeft: "110px"}}>
      <div>
      <img src={logo} alt="Logo" style={ { width: "150px",position: "absolute",top:" 20px"}} />
      </div>
      <div className="mb_nav_text">
      <h3 className="hotpink" style={{position: "fixed", right:"100px", top: "50px"}}>Energie besparen, zo doet u dat</h3>
      </div>
    </Container>
 
  </Navbar>
 
</>
  );
}

export default NavBar;
