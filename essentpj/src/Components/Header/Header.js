import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Header from '../../Images/hero_image.jpg'
import Login from '../../Images/login.jpg'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import './header.css'

function NavBar() {
  return (
    <>
    <div>
      <div className="spacing">
      <img className="image " src={Header} alt="Logo" />

      <div className="mb_img_text_container">
   <p className="mb_img_text1">Energie besparen, <br/>zo doet u dat</p>
   </div>
   </div>
      <div  className="item">
        <div className="fix">
                <img className="login_style"src={Login} alt="Logo" />
                <div className="login_text_style">Inloggen <span style={{color:"#178a9cbf"}}>Mijn Essent</span></div>
                </div>
 </div>
      </div>
  
 
</>
  );
}

export default NavBar;
