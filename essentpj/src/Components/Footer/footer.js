import React from 'react';
import './footer.css'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'



function footer() {
  return (
    <>
      <div className="footer_style">
          <h5 style={{color: "#969696", paddingBottom: "15px"}}>Copyright 2015 Essent</h5>
        <p>
        Alle aanbiedingen van Essent Energie Verkoop Nederland BV (EEVN) worden gemaakt onder voorbehoud van goedkeuring van het
management.
        </p>
        <p>
        U ontvangt deze productmail als klant van Essent. Via Mijn Energie Online kunt u uw gegevens en e-mailvoorkeuren wijzigen.
        </p>
        <p>
        Deze e-mail is een uitgave van Essent Energie Verkoop Nederland B.V. Postbus 1484, 5200 BM 's-Hertogenbosch.
        </p>
        <h5>
        <a href="/">Mijn Essent</a>&nbsp;|&nbsp;
        <a href="/">Privacy </a>&nbsp;|&nbsp;
        <a href="/">Copyright </a>&nbsp;|&nbsp;
        <a href="/">Disclaimer </a>&nbsp;|&nbsp;
        <a href="/">Email Preferences</a>&nbsp;|&nbsp;
        <a href="/">Unsubscribe </a>&nbsp;|&nbsp;
        <a href="/">Bekijk deze email online</a>
                      </h5>
      </div>

</>
  );
}

export default footer;
