import React from 'react';
import NavBar from './Components/NavBar/Nav'
import Header from './Components/Header/Header'
import Body from './Components/Body/body'
import Footer from './Components/Footer/footer'

function App() {
  return (
    <>
    <NavBar/>
    <Header/>
    <Body/>
    <Footer/>
</>
  );
}

export default App;
